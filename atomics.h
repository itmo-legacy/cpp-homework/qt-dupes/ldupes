#pragma once

#ifdef __cplusplus

#include <atomic>
using std::atomic_bool;

#else

#include <stdatomic.h>

#endif

